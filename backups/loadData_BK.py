import numpy as np
import csv
import os
import math
from six.moves import cPickle as pickle

def loadCsvData(path):
    dataset = np.ndarray(shape=(0, 431), dtype=np.float32)
    with open(path+'.csv', 'r') as csvfile:
        datareader = csv.reader(csvfile, delimiter=',')
        rownr = 0
        ignoreFirstRow = True
        for row in datareader:

            if ignoreFirstRow:
                ignoreFirstRow = False
                continue

            if rownr % 1000 == 0:
                print('Row nr', rownr)

            for i in range(len(row)):
                row[i] = float(row[i].replace(' ', ''))
            dataset = np.vstack((dataset, row))
            rownr += 1
            
    return dataset

def maybe_pickle(dataFile, force=False):
    set_filename = dataFile+'.pickle'
    if os.path.exists(set_filename) and not force:
        # You may override by setting force=True.
        print('%s already present - Skipping pickling.' % set_filename)
    else:
        print('Pickling %s.' % set_filename)
        dataset = loadCsvData(dataFile)
        try:
            with open(set_filename, 'wb') as f:
                pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
        except Exception as e:
            print('Unable to save data to', set_filename, ':', e)

    return set_filename

def readLabels(labelpath, startTimeMs, endTimeMs, timestep):
    lookAtLabelList = ['look_down', 'look_patient'] # TESTING
    # lookAtLabelList = ['look_down', 'look_patient', 'look_top_left', 'look_top_right', 'look_down_left', 'look_down_right']

    lookAt_data = np.empty((0, 3))
    with open(labelpath+'.txt', 'r') as csvfile:
        labelreader = csv.reader(csvfile, delimiter='\t')
        rownr = 0
        for row in labelreader:
            if row[0] == "LookAtPatient":
                if row[4] != 'look_patient': # TESTING
                    row[4] = 'look_down'
                lookAt_data = np.vstack((lookAt_data, [float(row[2]), float(row[3]), lookAtLabelList.index(row[4])]))

    # fix gaps
    for i in range(len(lookAt_data)-1):
        lookAt_data[i][1] = lookAt_data[i+1][0]

    data = {}
    data['lookAt'] = []

    for step in np.arange(startTimeMs, endTimeMs, timestep):
        # lookAt_data
        found = False
        for i in range(np.shape(lookAt_data)[0]):
            if step >= lookAt_data[i][0] and step < lookAt_data[i][1]:
                #data['lookAt'].append(int(lookAt_data[i][2]))
                data['lookAt'].append((np.arange(len(lookAtLabelList)) == int(lookAt_data[i][2])).astype(np.float32))
                found = True
                break
        if not found:
            print("meh")

    return data

def randomize(dataset, labels):
    permutation = np.random.permutation(labels.shape[0])
    shuffled_dataset = dataset[permutation,:]
    shuffled_labels = labels[permutation]
    return shuffled_dataset, shuffled_labels

def normalizeOpenFaceData(data):
    # frame, -- 1..n (int)
    # timestamp, -- 0..n*timestamp (n*0.02) (real)
    # confidence, -- 0..1 (real)
    data[:, [2]] -= 0.5
    # success, -- 0/1 (bool)
    data[:, [3]] -= 0.5
    # gaze_0_x, gaze_0_y, gaze_0_z, gaze_1_x, gaze_1_y, gaze_1_z, -- [-1,1] (real)
    data[:, [4,5,6,7,8,9]] /= 2.0 # normalize to [-0.5, 0.5] -- ~0.5 std
    # pose_Tx, pose_Ty, pose_Tz,
    # pose_Rx, pose_Ry, pose_Rz, -- [-pi,pi] (real)
    data[:, [13,14,15]] /= 2.0*math.pi # normalize radians to [-0.5,0.5] -- ~0.5 std
    # x_0..67, y_0..67, X_0..67, Y_0..67, Z_0..67,
    # p_scale, 
    # p_rx, p_ry, p_rz,
    # p_tx, p_ty,
    # p_0..33,
    # AU01_r, AU02_r, AU04_r, AU05_r, AU06_r, AU07_r, AU09_r, AU10_r, AU12_r, AU14_r, AU15_r, AU17_r, AU20_r, AU23_r, AU25_r, AU26_r, AU45_r, AU01_c, AU02_c, AU04_c, AU05_c, AU06_c, AU07_c, AU09_c, AU10_c, AU12_c, AU14_c, AU15_c, AU17_c, AU20_c, AU23_c, AU25_c, AU26_c, AU28_c, AU45_c
    return data

def lookAtDataset(data, labels, timeWindow, rnd):
    # lookAt dataset uses gaze_0_x, gaze_0_y, gaze_0_z, gaze_1_x, gaze_1_y, gaze_1_z, pose_Rx, pose_Ry, pose_Rz
    # therefore: 9 values
    usefulColIndices = [4,5,6,7,8,9,13,14,15]
    # usefulColIndices = [4,5,6,7,8,9] # pretty bad
    # usefulColIndices = [2,3,4,5,6,7,8,9,13,14,15] # not so great
    dataset = np.ndarray(shape=(len(data)-timeWindow, len(usefulColIndices)*timeWindow), dtype=np.float32)
    labelset =  np.ndarray(shape=(len(data)-timeWindow, len(labels['lookAt'][0])), dtype=np.float32)
    usefulCols = data[:,usefulColIndices] # get useful columns for this dataset

    dataStat = np.zeros(shape=(len(labels['lookAt'][0])))
    dataStat_copy = np.zeros(shape=(len(labels['lookAt'][0])))
    for i in range(len(data)-timeWindow):
        dataset[i, :] = np.reshape(usefulCols[i:(i+timeWindow), :], (1, int(len(usefulColIndices)*timeWindow))) # reshape into row vector
        labelset[i, :] = labels['lookAt'][i+int(timeWindow/2)] # get label in the middle
        dataStat[np.argmax(labelset[i, :])] += 1
        dataStat_copy[np.argmax(labelset[i, :])] += 1

    # # balance dataset by multiplying scarce data
    # print('Label stats:', dataStat)
    # statMax = np.max(dataStat)

    # copiesData = []
    # copiesLabels = []
    # for i in range(len(dataset)):
    #     lab = np.argmax(labelset[i,:])
        
    #     # add sufficient copies at the end
    #     copiesToAdd = (statMax-dataStat[lab]) / dataStat[lab]
    #     copiesToAddRemainder = copiesToAdd - math.floor(copiesToAdd)
    #     copiesToAdd = math.floor(copiesToAdd)

    #     if np.random.uniform() < copiesToAddRemainder:
    #         copiesToAdd += 1

    #     for j in range(copiesToAdd):
    #         copiesData.append(dataset[i,:])
    #         copiesLabels.append(labelset[i,:])
    #         dataStat_copy[lab] += 1

    # dataset = np.vstack((dataset, copiesData))
    # labelset = np.vstack((labelset, copiesLabels))

    print('Balanced label stats:', dataStat_copy)

    if rnd:
        return randomize(dataset, labelset)
    else:
        return dataset, labelset

def buildDatasets(path, startTimeMs, endTimeMs, timeWindow, tag='', force=False, rnd=True):
    dataset_name = maybe_pickle('./data/00020')

    data = None
    labels = None
    timestep = None

    # read data
    try:
        with open(dataset_name, 'rb') as f:
            data = pickle.load(f)

            # get time step
            timestep = (data[1][1] - data[0][1])*1000.0
            print('timestep', timestep)
            print(data.shape)
            print(np.sum(data[:,3]),'/',data.shape[0])

            labels = readLabels(path, startTimeMs=startTimeMs, endTimeMs=endTimeMs, timestep=timestep)

    except Exception as e:
        print(e)


    data = data[int(startTimeMs/timestep):int(endTimeMs/timestep+1), :]
    # sanity check
    print('Checking data/label size: ', len(data), ' vs ', len(labels['lookAt']))

    # normalize each type of data in the dataset
    data = normalizeOpenFaceData(data)

    # --------------------------------------------------------------------------------------- #
    # create lookAt dataset
    set_filename = path+'_lookAtdataset_'+tag+'_w'+str(timeWindow)+'.pickle'

    # check if pickled dataset exists
    if os.path.exists(set_filename) and not force:
        # You may override by setting force=True.
        print('%s already present - Skipping pickling.' % set_filename)
    else:
        print('Pickling %s.' % set_filename)
        
        dataset = lookAtDataset(data, labels, timeWindow=timeWindow, rnd=rnd) # time window in number of frames (depends on timestep) # timestep is usually 20ms for OpenFace data
        try:
            with open(set_filename, 'wb') as f:
                pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
                print('Saved',set_filename,'containing',len(dataset[0]),'samples')
        except Exception as e:
            print('Unable to save data to', set_filename, ':', e)
    # --------------------------------------------------------------------------------------- #

if __name__ == "__main__":
    #buildDatasets('./data/00020', startTimeMs=0, endTimeMs=185316, tag='train')
    timeWindow = 100
    buildDatasets('./data/00020', startTimeMs=0, endTimeMs=301780, timeWindow=timeWindow, tag='train')
    buildDatasets('./data/00020', startTimeMs=301780, endTimeMs=322260, timeWindow=timeWindow, tag='valid', rnd=False)
    buildDatasets('./data/00020', startTimeMs=322260, endTimeMs=358030, timeWindow=timeWindow, tag='test', rnd=False)
    #buildDatasets('./data/00020', startTimeMs=0, endTimeMs=301780, timeWindow=timeWindow, tag='test', rnd=False)

    