import numpy as np

def writeELANFile(entrytags, elanFile, videoPath, videoFileName):

    nrAnnotations = 0
    for tag, entries in entrytags.items():
        nrAnnotations += len(entries[entries[:,0] != np.array('None')])

    with open(elanFile, 'w') as f:
        f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        f.write('<ANNOTATION_DOCUMENT AUTHOR="" DATE="2017-05-12T16:58:56+01:00" FORMAT="2.8" VERSION="2.8" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.mpi.nl/tools/elan/EAFv2.8.xsd">\n')
        f.write('    <HEADER MEDIA_FILE="" TIME_UNITS="milliseconds">\n')
        f.write('        <MEDIA_DESCRIPTOR MEDIA_URL="'+videoPath+'" MIME_TYPE="video/mp4" RELATIVE_MEDIA_URL="./'+videoFileName+'"/>\n')
        f.write('        <PROPERTY NAME="URN">urn:nl-mpi-tools-elan-eaf:fbb10b00-3879-4e36-b3da-af52bfb986a6</PROPERTY>\n')
        f.write('        <PROPERTY NAME="lastUsedAnnotationId">'+str(nrAnnotations)+'</PROPERTY>\n')
        f.write('    </HEADER>\n')
        f.write('    <TIME_ORDER>\n')

        tsIndex = 1

        for tag, entries in entrytags.items():
            for i in range(len(entries)):
                if entries[i][0] == 'None':
                    continue
                f.write('       <TIME_SLOT TIME_SLOT_ID="ts'+str(tsIndex)+'" TIME_VALUE="'+str(entries[i][1])+'"/>\n')
                tsIndex += 1
                f.write('       <TIME_SLOT TIME_SLOT_ID="ts'+str(tsIndex)+'" TIME_VALUE="'+str(entries[i][2])+'"/>\n')
                tsIndex += 1

        f.write('    </TIME_ORDER>\n')
        

        aIndex = 1
        tsIndex = 1

        for tag, entries in entrytags.items():
            f.write('    <TIER LINGUISTIC_TYPE_REF="default-lt" TIER_ID="'+tag+'">\n')
            for i in range(len(entries)):
                if entries[i][0] == 'None':
                    continue
                f.write('       <ANNOTATION>\n')
                f.write('           <ALIGNABLE_ANNOTATION ANNOTATION_ID="a'+str(aIndex)+'" TIME_SLOT_REF1="ts'+str(tsIndex)+'" TIME_SLOT_REF2="ts'+str(tsIndex+1)+'">\n')
                f.write('               <ANNOTATION_VALUE>'+entries[i][0]+'</ANNOTATION_VALUE>\n')
                f.write('           </ALIGNABLE_ANNOTATION>\n')
                f.write('       </ANNOTATION>\n')
                aIndex += 1
                tsIndex += 2

            f.write('    </TIER>\n')
        f.write('    <LINGUISTIC_TYPE GRAPHIC_REFERENCES="false" LINGUISTIC_TYPE_ID="default-lt" TIME_ALIGNABLE="true"/>\n')
        f.write('    <CONSTRAINT DESCRIPTION="Time subdivision of parent annotation\'s time interval, no time gaps allowed within this interval" STEREOTYPE="Time_Subdivision"/>\n')
        f.write('    <CONSTRAINT DESCRIPTION="Symbolic subdivision of a parent annotation. Annotations refering to the same parent are ordered" STEREOTYPE="Symbolic_Subdivision"/>\n')
        f.write('    <CONSTRAINT DESCRIPTION="1-1 association with a parent annotation" STEREOTYPE="Symbolic_Association"/>\n')
        f.write('    <CONSTRAINT DESCRIPTION="Time alignable annotations within the parent annotation\'s time interval, gaps are allowed" STEREOTYPE="Included_In"/>\n')
        f.write('</ANNOTATION_DOCUMENT>\n')

def prepareELANLabel(predictions, annotations, startTime, videoPath, videoFileName, timeStep=20):
    entries = np.ndarray(shape=(0, 3), dtype=np.float32) # record nr, [annotation, startTime, endTime]

    crtLabel = np.argmax(predictions[0])
    crtTime = startTime
    lastTime = startTime
    for i in range(1, len(predictions)):
        predLabel = np.argmax(predictions[i])
        if (crtLabel != predLabel) or (i == len(predictions)-1):
            # add entry
            entry = [annotations[crtLabel], lastTime, crtTime]
            entries = np.vstack((entries, entry))
            lastTime = crtTime
            crtLabel = predLabel
        crtTime += timeStep

    return entries

def prepareELANSuccess(predictions, annotations, startTime, videoPath, videoFileName, timeStep=20):
    entries = np.ndarray(shape=(0, 3), dtype=np.float32) # record nr, [annotation, startTime, endTime]

    crtLabel = np.sum(predictions[0]) == len(predictions[0])
    crtTime = startTime
    lastTime = startTime
    for i in range(1, len(predictions)):
        predLabel = np.sum(predictions[i]) == len(predictions[i])
        if (crtLabel != predLabel) or (i == len(predictions)-1):
            # add entry
            entry = [annotations[crtLabel], lastTime, crtTime]
            entries = np.vstack((entries, entry))
            lastTime = crtTime
            crtLabel = predLabel
        crtTime += timeStep

    return entries

def exportELAN(elanFile, entrytags, videoPath, videoFileName):
    writeELANFile(entrytags, elanFile, videoPath, videoFileName)



if __name__ == '__main__':
    lookAtLabelList = ['look_down', 'look_patient', 'look_top_left', 'look_top_right', 'look_down_left', 'look_down_right']

    testPred = np.zeros((10, 6))
    testPred[0,0] = 1
    testPred[1,0] = 1
    testPred[2,1] = 1
    testPred[3,1] = 1
    testPred[4,2] = 1
    testPred[5,2] = 1
    testPred[6,3] = 1
    testPred[7,3] = 1
    testPred[8,4] = 1
    testPred[9,5] = 1
    
    videoFileName = 'test.mp4'
    videoPath = 'file:///media/mike/5EED132D43AA74B1/sombrero_data/data/lucie/2017/gros plan/emilie/'+videoFileName

    testDict = {}
    testDict['test'] = prepareELANLabel(predictions=testPred, annotations=lookAtLabelList, startTime=100, videoPath=videoPath, videoFileName=videoFileName)

    exportELAN(elanFile=videoFileName+'.eaf', entrytags=testDict, videoPath=videoPath, videoFileName=videoFileName)