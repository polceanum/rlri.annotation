import numpy as np
import csv
import os
import math
from six.moves import cPickle as pickle

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from operator import itemgetter

from scipy.linalg import expm

def rot_euler(v, xyz):
    ''' Rotate vector v (or array of vectors) by the euler angles xyz '''
    # https://stackoverflow.com/questions/6802577/python-rotation-of-3d-vector
    for theta, axis in zip(xyz, np.eye(3)):
        v = np.dot(np.array(v), expm(np.cross(np.eye(3), axis*-theta)))
    return v

enablePlot = False

def loadCsvData(path, dataSize):
    dataset = np.ndarray(shape=(0, dataSize), dtype=np.float32)
    with open(path+'.csv', 'r') as csvfile:
        datareader = csv.reader(csvfile, delimiter=',')
        rownr = 0
        ignoreFirstRow = True
        for row in datareader:

            if ignoreFirstRow:
                ignoreFirstRow = False
                continue

            if rownr % 1000 == 0:
                print('Row nr', rownr)

            for i in range(len(row)):
                row[i] = float(row[i].replace(' ', ''))
            dataset = np.vstack((dataset, row))
            rownr += 1
            
    return dataset

def maybe_pickle(dataFile, dataSize, force=False):
    set_filename = dataFile+'.pickle'
    if os.path.exists(set_filename) and not force:
        # You may override by setting force=True.
        print('%s already present - Skipping pickling.' % set_filename)
    else:
        print('Pickling %s.' % set_filename)
        dataset = loadCsvData(dataFile, dataSize)
        try:
            with open(set_filename, 'wb') as f:
                pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
        except Exception as e:
            print('Unable to save data to', set_filename, ':', e)

    return set_filename

def readLabels(labelpath, startTimeMs, endTimeMs, timestep, fixGaps=False):
    #lookAtLabelList = ['look_down', 'look_patient'] # TESTING
    # lookAtLabelList = ['look_down', 'look_patient', 'look_top_left', 'look_top_right', 'look_down_left', 'look_down_right']
    lookAtLabelList = ['None', 'look_patient']
    headNodLabelList = ['None', 'nod']
    

    lookAt_data = np.empty((0, 3))
    headNod_data = np.empty((0, 3))
    with open(labelpath+'.txt', 'r') as csvfile:
        labelreader = csv.reader(csvfile, delimiter='\t')
        rownr = 0
        for row in labelreader:
            if row[0] == "LookAtPatient":
                if row[4] != 'look_patient': # TESTING
                    row[4] = 'None'
                lookAt_data = np.vstack((lookAt_data, [float(row[2]), float(row[3]), lookAtLabelList.index(row[4])]))
            elif row[0] == "HeadNod":
                headNod_data = np.vstack((headNod_data, [float(row[2]), float(row[3]), headNodLabelList.index(row[4])]))

    # either close gaps or insert Nones
    if fixGaps:
        # fix gaps
        for i in range(len(lookAt_data)-1):
            lookAt_data[i][1] = lookAt_data[i+1][0]
        for i in range(len(headNod_data)-1):
            headNod_data[i][1] = headNod_data[i+1][0]
    else:
        # fill in with 'None'
        if len(lookAt_data) > 0 and lookAt_data[0][0] > 0:
            lookAt_data = np.insert(lookAt_data, 0, [0, lookAt_data[0][0], lookAtLabelList.index('None')], axis=0) # insert interval at this position
        i = 0
        while i < len(lookAt_data)-1:
            if lookAt_data[i][1] < lookAt_data[i+1][0]:
                lookAt_data = np.insert(lookAt_data, i+1, [lookAt_data[i][1], lookAt_data[i+1][0], lookAtLabelList.index('None')], axis=0) # insert interval at this position
            i += 1
        if len(lookAt_data) > 0 and lookAt_data[len(lookAt_data)-1][1] < endTimeMs:
            lookAt_data = np.append(lookAt_data, [[lookAt_data[len(lookAt_data)-1][1], endTimeMs, lookAtLabelList.index('None')]], axis=0) # insert interval at this position

        # fill in with 'None'
        if len(headNod_data) > 0 and headNod_data[0][0] > 0:
            headNod_data = np.insert(headNod_data, 0, [0, headNod_data[0][0], headNodLabelList.index('None')], axis=0) # insert interval at this position
        i = 0
        while i < len(headNod_data)-1:
            if headNod_data[i][1] < headNod_data[i+1][0]:
                headNod_data = np.insert(headNod_data, i+1, [headNod_data[i][1], headNod_data[i+1][0], headNodLabelList.index('None')], axis=0) # insert interval at this position
            i += 1
        if len(headNod_data) > 0 and headNod_data[len(headNod_data)-1][1] < endTimeMs:
            headNod_data = np.append(headNod_data, [[headNod_data[len(headNod_data)-1][1], endTimeMs, headNodLabelList.index('None')]], axis=0) # insert interval at this position

    data = {}
    data['lookAt'] = []
    data['headNod'] = []

    for step in np.arange(startTimeMs, endTimeMs, timestep):
        # lookAt_data
        found = False
        for i in range(np.shape(lookAt_data)[0]):
            if step >= lookAt_data[i][0] and step < lookAt_data[i][1]:
                #data['lookAt'].append(int(lookAt_data[i][2]))
                data['lookAt'].append((np.arange(len(lookAtLabelList)) == int(lookAt_data[i][2])).astype(np.float32))
                found = True
                break
        # if not found:
        #     print("meh lookAt")

        found = False
        for i in range(np.shape(headNod_data)[0]):
            if step >= headNod_data[i][0] and step < headNod_data[i][1]:
                data['headNod'].append((np.arange(len(headNodLabelList)) == int(headNod_data[i][2])).astype(np.float32))
                found = True
                break
        # if not found:
        #     print("meh headNod")

    return data

def randomize(dataset, labels):
    permutation = np.random.permutation(labels.shape[0])
    shuffled_dataset = dataset[permutation,:]
    shuffled_labels = labels[permutation]
    return shuffled_dataset, shuffled_labels

def normalizeOpenFaceData(data):
    # frame, -- 1..n (int)
    # timestamp, -- 0..n*timestamp (n*0.02) (real)
    # confidence, -- 0..1 (real)
    # success, -- 0/1 (bool)
    # gaze_0_x, gaze_0_y, gaze_0_z, gaze_1_x, gaze_1_y, gaze_1_z, -- [-1,1] (real)
    #data[:, [4,5,6,7,8,9]] /= 2.0 # normalize to [-0.5, 0.5] -- ~0.5 std
    # pose_Tx, pose_Ty, pose_Tz,
    # pose_Rx, pose_Ry, pose_Rz, -- [-pi,pi] (real)
    #data[:, [13,14,15]] /= 2.0*math.pi # normalize radians to [-0.5,0.5] -- ~0.5 std
    # x_0..67, y_0..67, X_0..67, Y_0..67, Z_0..67,
    # p_scale, 
    # p_rx, p_ry, p_rz,
    # p_tx, p_ty,
    # p_0..33,
    # AU01_r, AU02_r, AU04_r, AU05_r, AU06_r, AU07_r, AU09_r, AU10_r, AU12_r, AU14_r, AU15_r, AU17_r, AU20_r, AU23_r, AU25_r, AU26_r, AU45_r, AU01_c, AU02_c, AU04_c, AU05_c, AU06_c, AU07_c, AU09_c, AU10_c, AU12_c, AU14_c, AU15_c, AU17_c, AU20_c, AU23_c, AU25_c, AU26_c, AU28_c, AU45_c
    
    #data[:,4:] = (data[:,4:] - np.mean(data, axis=0)[4:]) / (np.max(data, axis=0)[4:] - np.min(data, axis=0)[4:]) #(2.0*np.std(data, axis=0)[4:])

    data[:, 431+1] = (data[:,431+1] - data[:,431+3]/2.0) / (data[:,431+3])
    data[:, 431+2] = (data[:,431+2] - data[:,431+4]/2.0) / (data[:,431+4])

    data[:, 431+5] = (data[:,431+5] - data[:,431+7]/2.0) / (data[:,431+7])
    data[:, 431+6] = (data[:,431+6] - data[:,431+8]/2.0) / (data[:,431+8])

    # histData = data[:, [13, 14, 15, 431+1, 431+2]]
    # H, edges = np.histogramdd(histData, bins = 10)
    # print(H.shape, edges[0].size, edges[1].size, edges[2].size)

    # for i in range(10):
    #     for j in range(10):
    #         for k in range(10):
    #             sm = np.sum(H[i,j,k])
    #             if sm > 10:
    #                 print((i,j,k),'sum:',sm)
    #                 fig = plt.figure()
    #                 plt.imshow(H[i, j, k], interpolation='nearest', origin='low', extent=[edges[3][0], edges[3][-1], edges[4][0], edges[4][-1]])
    #                 plt.show()


    data[:, 4:16] = (data[:,4:16] - np.mean(data, axis=0)[4:16]) / (np.max(data, axis=0)[4:16] - np.min(data, axis=0)[4:16])

    return data

def lookAtDataset(data, labels, timeWindow, rnd, useLabels):
    # lookAt dataset uses gaze_0_x, gaze_0_y, gaze_0_z, gaze_1_x, gaze_1_y, gaze_1_z, pose_Rx, pose_Ry, pose_Rz
    # therefore: 9 values
    # usefulColIndices = [4,5,6,7,8,9,13,14,15]
    # usefulColIndices = [4,5,6,7,8,9] # pretty bad
    # usefulColIndices = [2,3,4,5,6,7,8,9,13,14,15] # not so great

    usefulColIndices = [13, 14, 15, 431+1, 431+2, 431+5, 431+6] # use new eye detection
    
    # if useLabels:
    #     dataset = np.ndarray(shape=(int(np.sum(data[:,3]))-timeWindow, len(usefulColIndices)*timeWindow), dtype=np.float32)
    # else:
    #     dataset = np.ndarray(shape=(len(data)-timeWindow, len(usefulColIndices)*timeWindow), dtype=np.float32)
    dataset = np.empty((len(data)-timeWindow, len(usefulColIndices)*timeWindow), dtype=np.float32)

    usefulCols = data[:,usefulColIndices] # get useful columns for this dataset
    if not useLabels or len(labels['lookAt']) == 0:
        for i in range(len(data)-timeWindow):
            #dataset[i, :] = np.reshape(usefulCols[i:(i+timeWindow), :], (1, int(len(usefulColIndices)*timeWindow))) # reshape into row vector

            dataset[i, :] = np.reshape(usefulCols[i:(i+timeWindow), :], (1, int(len(usefulColIndices)*timeWindow))) # reshape into row vector
        return dataset

    #labelset =  np.ndarray(shape=(int(np.sum(data[:,3]))-timeWindow, len(labels['lookAt'][0])), dtype=np.float32)
    labelset = np.empty((len(data)-timeWindow, len(labels['lookAt'][0])), dtype=np.float32)

    dataStat = np.zeros(shape=(len(labels['lookAt'][0])))
    dataStat_copy = np.zeros(shape=(len(labels['lookAt'][0])))

    removeCount = 0
    newDataSize = 0
    for i in range(len(data)-timeWindow):
        if np.sum(data[i:(i+timeWindow), 3]) < timeWindow*0.8:
            removeCount += 1
            continue # drop windows with frames that have 0 success score
        # dataset[i, :] = np.reshape(usefulCols[ii:(ii+timeWindow), :], (1, int(len(usefulColIndices)*timeWindow))) # reshape into row vector
        # labelset[i, :] = labels['lookAt'][ii+int(timeWindow/2)] # get label in the middle

        dataset[newDataSize, :] = np.reshape(usefulCols[i:(i+timeWindow), :], (1, int(len(usefulColIndices)*timeWindow))) # reshape into row vector
        labelset[newDataSize, :] = labels['lookAt'][i+int(timeWindow/2)] # get label in the middle

        dataStat[np.argmax(labelset[newDataSize, :])] += 1
        dataStat_copy[np.argmax(labelset[newDataSize, :])] += 1
        newDataSize += 1
    dataset = np.resize(dataset, (newDataSize, dataset.shape[1]))
    labelset = np.resize(labelset, (newDataSize, labelset.shape[1]))

    print('Removed',removeCount,'of',len(data))

    if rnd:
        # # balance dataset by multiplying scarce data
        # print('Label stats:', dataStat)
        # statMax = np.max(dataStat)

        # copiesData = []
        # copiesLabels = []
        # for i in range(len(dataset)):
        #     lab = np.argmax(labelset[i,:])
            
        #     # add sufficient copies at the end
        #     copiesToAdd = (statMax-dataStat[lab]) / dataStat[lab]
        #     copiesToAddRemainder = copiesToAdd - math.floor(copiesToAdd)
        #     copiesToAdd = math.floor(copiesToAdd)

        #     if np.random.uniform() < copiesToAddRemainder:
        #         copiesToAdd += 1

        #     for j in range(copiesToAdd):
        #         copiesData.append(dataset[i,:])
        #         copiesLabels.append(labelset[i,:])
        #         dataStat_copy[lab] += 1

        # dataset = np.vstack((dataset, copiesData))
        # labelset = np.vstack((labelset, copiesLabels))

        # print('Balanced label stats:', dataStat_copy)

    
        return randomize(dataset, labelset)
    else:
        return dataset, labelset

def headNodDataset(data, labels, timeWindow, rnd, useLabels):
    # lookAt dataset uses gaze_0_x, gaze_0_y, gaze_0_z, gaze_1_x, gaze_1_y, gaze_1_z, pose_Rx, pose_Ry, pose_Rz
    # therefore: 9 values
    # usefulColIndices = [4,5,6,7,8,9,13,14,15]
    # usefulColIndices = [4,5,6,7,8,9] # pretty bad
    # usefulColIndices = [2,3,4,5,6,7,8,9,13,14,15] # not so great

    usefulColIndices = [13, 14, 15] # use new eye detection
    
    # if useLabels:
    #     dataset = np.ndarray(shape=(int(np.sum(data[:,3]))-timeWindow, len(usefulColIndices)*timeWindow), dtype=np.float32)
    # else:
    #     dataset = np.ndarray(shape=(len(data)-timeWindow, len(usefulColIndices)*timeWindow), dtype=np.float32)
    dataset = np.empty((len(data)-timeWindow, len(usefulColIndices)*timeWindow), dtype=np.float32)

    usefulCols = data[:,usefulColIndices] # get useful columns for this dataset
    if not useLabels or len(labels['headNod']) == 0:
        for i in range(len(data)-timeWindow):
            #dataset[i, :] = np.reshape(usefulCols[i:(i+timeWindow), :], (1, int(len(usefulColIndices)*timeWindow))) # reshape into row vector

            dataset[i, :] = np.reshape(usefulCols[i:(i+timeWindow), :], (1, int(len(usefulColIndices)*timeWindow))) # reshape into row vector
        return dataset

    #labelset =  np.ndarray(shape=(int(np.sum(data[:,3]))-timeWindow, len(labels['lookAt'][0])), dtype=np.float32)
    labelset = np.empty((len(data)-timeWindow, len(labels['headNod'][0])), dtype=np.float32)

    dataStat = np.zeros(shape=(len(labels['headNod'][0])))
    dataStat_copy = np.zeros(shape=(len(labels['headNod'][0])))

    removeCount = 0
    newDataSize = 0
    for i in range(len(data)-timeWindow):
        if np.sum(data[i:(i+timeWindow), 3]) < timeWindow*0.8:
            removeCount += 1
            continue # drop windows with frames that have 0 success score
        # dataset[i, :] = np.reshape(usefulCols[ii:(ii+timeWindow), :], (1, int(len(usefulColIndices)*timeWindow))) # reshape into row vector
        # labelset[i, :] = labels['lookAt'][ii+int(timeWindow/2)] # get label in the middle

        dataset[newDataSize, :] = np.reshape(usefulCols[i:(i+timeWindow), :], (1, int(len(usefulColIndices)*timeWindow))) # reshape into row vector
        labelset[newDataSize, :] = labels['headNod'][i+int(timeWindow/2)] # get label in the middle

        dataStat[np.argmax(labelset[newDataSize, :])] += 1
        dataStat_copy[np.argmax(labelset[newDataSize, :])] += 1
        newDataSize += 1
    dataset = np.resize(dataset, (newDataSize, dataset.shape[1]))
    labelset = np.resize(labelset, (newDataSize, labelset.shape[1]))

    print('Removed',removeCount,'of',len(data))

    if rnd:
        # # balance dataset by multiplying scarce data
        # print('Label stats:', dataStat)
        # statMax = np.max(dataStat)

        # copiesData = []
        # copiesLabels = []
        # for i in range(len(dataset)):
        #     lab = np.argmax(labelset[i,:])
            
        #     # add sufficient copies at the end
        #     copiesToAdd = (statMax-dataStat[lab]) / dataStat[lab]
        #     copiesToAddRemainder = copiesToAdd - math.floor(copiesToAdd)
        #     copiesToAdd = math.floor(copiesToAdd)

        #     if np.random.uniform() < copiesToAddRemainder:
        #         copiesToAdd += 1

        #     for j in range(copiesToAdd):
        #         copiesData.append(dataset[i,:])
        #         copiesLabels.append(labelset[i,:])
        #         dataStat_copy[lab] += 1

        # dataset = np.vstack((dataset, copiesData))
        # labelset = np.vstack((labelset, copiesLabels))

        # print('Balanced label stats:', dataStat_copy)

    
        return randomize(dataset, labelset)
    else:
        return dataset, labelset

def successDataset(data, labels, timeWindow):
    # success score (0 / 1)
    usefulColIndices = [3]
    
    dataset = np.empty((len(data)-timeWindow, len(usefulColIndices)*timeWindow), dtype=np.float32)

    usefulCols = data[:,usefulColIndices] # get useful columns for this dataset
    
    for i in range(len(data)-timeWindow):
        dataset[i, :] = np.reshape(usefulCols[i:(i+timeWindow), :], (1, int(len(usefulColIndices)*timeWindow))) # reshape into row vector
    return dataset

def buildDatasets(path, startTimeMs, endTimeMs, timeWindow, tag='', datasetName='all', force=False, rnd=True, useLabels=True, fixGaps=False):
    dataset_name = maybe_pickle(path, dataSize=431)

    dataset_crop_name = maybe_pickle(path+'_crop', dataSize=9)

    data = None
    labels = None
    timestep = None

    
    # read data
    #try:
    #with open(dataset_name, 'rb') as f:
    with open(dataset_name, 'rb') as f, open(dataset_crop_name, 'rb') as f_crop:
        data = pickle.load(f)
        data_crop = pickle.load(f_crop)

        data = np.hstack((data, data_crop))

        # get time step
        timestep = (data[1][1] - data[0][1])*1000.0
        print('timestep', timestep)
        print(data.shape)
        print(np.sum(data[:,3]),'/',data.shape[0])

        if useLabels:
            labels = readLabels(path, startTimeMs=startTimeMs, endTimeMs=endTimeMs, timestep=timestep, fixGaps=fixGaps)

    # except Exception as e:
    #     print('Exception:',e)

    totalMean = data.mean(axis=0)
    totalStd = data.std(axis=0)
    print('mean confidence:', totalMean[2])
    print('mean success:', totalMean[3])

    print('0 -->',np.max(data), data.shape)
    # normalize each type of data in the dataset
    data = normalizeOpenFaceData(data)

    print('1 -->',np.max(data[:,4:]), data.shape)

    data = data[int(startTimeMs/timestep):int(endTimeMs/timestep+1), :]

    print('2 -->',np.max(data[:,4:]), data.shape)
    
    if useLabels:
        # sanity check
        print('Checking data/label size: ', len(data), ' vs ', len(labels['lookAt']))

    if enablePlot:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        # xs = data[:, 4]
        # ys = data[:, 5]
        # zs = data[:, 6] * 0

        # xs = data[:, 13]
        # ys = data[:, 14]
        # zs = data[:, 15]

        # limited_xs = []
        # limited_ys = []

        # limit_range = [0.1, -0.1, 0.0]

        # for i in range(len(data)):
        #     if np.abs(data[i, 13]-limit_range[0]) < 0.05 and np.abs(data[i, 14]-limit_range[1]) < 0.05 and np.abs(data[i, 15]-limit_range[2]) < 0.05:
        #         limited_xs.append(data[i, 431+1])
        #         limited_ys.append(data[i, 431+2])

        # xs = limited_xs
        # ys = limited_ys

        xs = data[:, 431+1]
        ys = data[:, 431+2]
        zs = data[:, 13]

        # xs = np.zeros(shape=(len(data), 1))
        # ys = np.zeros(shape=(len(data), 1))
        # zs = np.zeros(shape=(len(data), 1))
        # for i in range(len(data)):
        #     v = data[i,13:16]
        #     v = rot_euler([1.0/np.sqrt(3),1.0/np.sqrt(3),1.0/np.sqrt(3)], v)
        #     xs[i] = v[0]
        #     ys[i] = v[1]
        #     zs[i] = v[2]

        # for i in range(len(data)):
        #     if data[i,2] < 0.98 or data[i,3] == 0:
        #         xs[i] = 0
        #         ys[i] = 0
        #         zs[i] = 0
        
        colors = ['r', 'b']
        if useLabels:
            lablist = [int(i) for i in list(np.array(labels['lookAt'])[:,0])]
            #plt.scatter(xs, ys, c=list(itemgetter(*(lablist))(colors)), marker='x')
            plt.scatter(xs, ys, zs, c=list(itemgetter(*(lablist))(colors)), marker='x')
        else:
            plt.scatter(xs, ys, c='g', marker='x')

        #ax.scatter(xs, ys, zs, c=list(itemgetter(*(lablist))(colors)), marker='x')
        

        # ax.set_xlabel('X Label')
        # ax.set_ylabel('Y Label')
        #ax.set_zlabel('Z Label')
        plt.axis('equal')

        plt.show()

    

    # --------------------------------------------------------------------------------------- #
    # create lookAt dataset
    if datasetName=='lookAt' or datasetName=='all':
        set_filename = path+'_lookAtdataset_'+tag+'_w'+str(timeWindow)+'.pickle'

        # check if pickled dataset exists
        if os.path.exists(set_filename) and not force:
            # You may override by setting force=True.
            print('%s already present - Skipping pickling.' % set_filename)
        else:
            print('Pickling %s.' % set_filename)
            print('3 -->',np.max(data[:,4:]), data.shape)
            dataset = lookAtDataset(data, labels, timeWindow=timeWindow, rnd=rnd, useLabels=useLabels) # time window in number of frames (depends on timestep) # timestep is usually 20ms for OpenFace data
            if useLabels:
                print('4 -->',np.max(dataset[0]), dataset[0].shape)
            else:
                print('4 -->',np.max(dataset), dataset.shape)
            try:
                with open(set_filename, 'wb') as f:
                    pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
                    print('Saved',set_filename,'containing',len(dataset[0]),'samples')
            except Exception as e:
                print('Unable to save data to', set_filename, ':', e)
    # --------------------------------------------------------------------------------------- #

    # --------------------------------------------------------------------------------------- #
    # create HeadNod dataset
    # if datasetName=='headNod' or datasetName=='all':
    #     set_filename = path+'_headNod_'+tag+'_w'+str(timeWindow)+'.pickle'

    #     # check if pickled dataset exists
    #     if os.path.exists(set_filename) and not force:
    #         # You may override by setting force=True.
    #         print('%s already present - Skipping pickling.' % set_filename)
    #     else:
    #         print('Pickling %s.' % set_filename)
    #         print('3 -->',np.max(data[:,4:]), data.shape)
    #         dataset = headNodDataset(data, labels, timeWindow=timeWindow, rnd=rnd, useLabels=useLabels) # time window in number of frames (depends on timestep) # timestep is usually 20ms for OpenFace data
    #         if useLabels:
    #             print('4 -->',np.max(dataset[0]), dataset[0].shape)
    #         else:
    #             print('4 -->',np.max(dataset), dataset.shape)
    #         try:
    #             with open(set_filename, 'wb') as f:
    #                 pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
    #                 print('Saved',set_filename,'containing',len(dataset[0]),'samples')
    #         except Exception as e:
    #             print('Unable to save data to', set_filename, ':', e)
    # --------------------------------------------------------------------------------------- #

    # --------------------------------------------------------------------------------------- #
    if datasetName=='success' or datasetName=='all':
        if not rnd and not useLabels:
            # create success dataset
            set_filename = path+'_success_'+tag+'_w'+str(timeWindow)+'.pickle'

            # check if pickled dataset exists
            if os.path.exists(set_filename) and not force:
                # You may override by setting force=True.
                print('%s already present - Skipping pickling.' % set_filename)
            else:
                print('Pickling %s.' % set_filename)
                dataset = successDataset(data, labels, timeWindow=timeWindow) # time window in number of frames (depends on timestep) # timestep is usually 20ms for OpenFace data
                print('-->',np.max(dataset), dataset.shape)
                try:
                    with open(set_filename, 'wb') as f:
                        pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
                        print('Saved',set_filename,'containing',len(dataset[0]),'samples')
                except Exception as e:
                    print('Unable to save data to', set_filename, ':', e)
    # --------------------------------------------------------------------------------------- #

if __name__ == "__main__":
    #buildDatasets('./data/00020', startTimeMs=0, endTimeMs=185316, tag='train')
    timeWindow = 100

    # buildDatasets('./data/00020', startTimeMs=0, endTimeMs=126061, timeWindow=timeWindow, tag='train', fixGaps=True)
    # buildDatasets('./data/00020', startTimeMs=126060, endTimeMs=213381, timeWindow=timeWindow, tag='valid', rnd=False, fixGaps=True)
    # buildDatasets('./data/00020', startTimeMs=213380, endTimeMs=358021, timeWindow=timeWindow, tag='test', rnd=False, fixGaps=True)

    # buildDatasets('./data/00046', startTimeMs=0, endTimeMs=345321, timeWindow=timeWindow, tag='train')
    # buildDatasets('./data/00046', startTimeMs=345320, endTimeMs=531461, timeWindow=timeWindow, tag='valid')
    # buildDatasets('./data/00046', startTimeMs=531460, endTimeMs=635501, timeWindow=timeWindow, tag='test')

    slices = 60
    for i in range(slices):
        slice00020 = ((358021 // slices) // 20) * 20 # make sure it's divisible by 20 (timestep)
        buildDatasets('./data/00020', startTimeMs=i*slice00020, endTimeMs=(i+1)*slice00020+1, timeWindow=timeWindow, tag='slice'+str(i), fixGaps=True)
        #buildDatasets('./data/00020', startTimeMs=i*slice00020, endTimeMs=(i+1)*slice00020+1, timeWindow=timeWindow, tag='slice'+str(i), datasetName='headNod')

        slice00046 = ((635501 // slices) // 20) * 20 # make sure it's divisible by 20 (timestep)
        buildDatasets('./data/00046', startTimeMs=i*slice00046, endTimeMs=(i+1)*slice00046+1, timeWindow=timeWindow, tag='slice'+str(i))


    # buildDatasets('./data/00020', startTimeMs=0, endTimeMs=443940, timeWindow=timeWindow, tag='testall', rnd=False, useLabels=False)

    #buildDatasets('./data/00020', startTimeMs=0, endTimeMs=443940, timeWindow=timeWindow, tag='testall', rnd=False, useLabels=False, datasetName='headNod')

    # buildDatasets('./data/00021', startTimeMs=0, endTimeMs=1395820, timeWindow=timeWindow, tag='testall', rnd=False, useLabels=False)

    # buildDatasets('./data/00021', startTimeMs=0, endTimeMs=1395820, timeWindow=timeWindow, tag='testall', rnd=False, useLabels=False, datasetName='headNod')

    # buildDatasets('./data/00046', startTimeMs=0, endTimeMs=634980, timeWindow=timeWindow, tag='testall', rnd=False, useLabels=False)

    # buildDatasets('./data/00003', startTimeMs=0, endTimeMs=619140, timeWindow=timeWindow, tag='testall', rnd=False, useLabels=False)

    